package com.tongji.pintu.backend.mapper;

import com.tongji.pintu.backend.domain.Favorite;
import com.tongji.pintu.backend.domain.Favorite;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.apache.ibatis.annotations.Param;

@Mapper
@Component
public interface CollectMapper {
    Integer collectBoard (@Param("favorite") Favorite favorite);
    Integer cancelCollection (@Param("favorite") Favorite favorite);
    Boolean isCollected(@Param("userId") Integer userId, @Param("boardId") Integer boardId);
}
