package com.tongji.pintu.backend.mapper;

import com.tongji.pintu.backend.domain.Board;
import com.tongji.pintu.backend.domain.News;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface NewMapper {
    public List<News> getNews(@Param("idList") List<Integer> idList, Integer pageCursor, Integer pageSize);

}
