package com.tongji.pintu.backend.mapper;

import com.tongji.pintu.backend.domain.Attend;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
@Component
public interface AttendMapper {
    Integer exitBoard (@Param("attend") Attend attend);
    Integer attendBoard (@Param("attend") Attend attend);
    Boolean isAttended(@Param("userId") Integer userId, @Param("boardId") Integer boardId);
}
