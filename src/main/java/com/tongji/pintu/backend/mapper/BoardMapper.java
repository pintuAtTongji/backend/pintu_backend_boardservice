package com.tongji.pintu.backend.mapper;

import com.tongji.pintu.backend.domain.Board;
import com.tongji.pintu.backend.domain.Key;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
@Component
public interface BoardMapper {


   Integer publish (@Param("board") Board board);

    Integer deleteBoard(int id);

    Integer updateBoard(Integer id,@Param("board")Board board);

    Board getBoard(int id);

    List<Board> getAttendBoard (Integer userId);

    List<Board> getCollectBoard (Integer userId);

    List<Board> getCreateBoard (int userId);

    List<Board> searchWithTitle (Key key);

    List<Board> searchWithContent (Key key);

    List<Integer> getAttender (int id);
}
