package com.tongji.pintu.backend.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface UserMapper {
    Integer addUser(@Param("id") Integer id);
    boolean isExist(@Param("id") Integer id);
}
