package com.tongji.pintu.backend.domain;

import io.swagger.annotations.ApiModelProperty;

public class Favorite {
    @ApiModelProperty(value = "图板id", position = 1)
    private Integer boardId;

    @ApiModelProperty(value = "收藏者id", position = 2)
    private Integer userId;

    public Favorite() {
    }

    public Favorite(Integer boardId, Integer userId) {
        this.boardId = boardId;
        this.userId = userId;
    }

    public Integer getBoardId() {
        return boardId;
    }

    public void setBoardId(Integer boardId) {
        this.boardId = boardId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}