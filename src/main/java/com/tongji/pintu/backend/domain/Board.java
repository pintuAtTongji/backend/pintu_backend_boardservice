package com.tongji.pintu.backend.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@ApiModel("Board Dao")
public class Board {
    @ApiModelProperty(value = "图板id", position = 1)
    private Integer id;

    @ApiModelProperty(value = "发起者id", position = 2)
    private Integer userId;

    @ApiModelProperty(value = "标题", position = 3)
    private String title;

    @ApiModelProperty(value = "内容", position = 4)
    private String content;

    @ApiModelProperty(value = "参与人数", position = 5)
    private Integer userCount;

    @ApiModelProperty(value = "创建时间",example = "2019-10-10 10:10:10",position = 6)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @ApiModelProperty(value = "图板的图片", position = 6)
    private String relatedImage;

    public Board(Integer id, Integer userId, String title, String content, Integer userCount, Date gmtCreate, String relatedImage) {
        this.id = id;
        this.userId= userId;
        this.title = title;
        this.content = content;
        this.userCount = userCount;
        this.gmtCreate = gmtCreate;
        this.relatedImage = relatedImage;
    }

    public Board() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content =content;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }


    public String getRelatedImage() {
        return relatedImage;
    }

    public void setRelatedImage(String relatedImage) {
        this.relatedImage = relatedImage;
    }
}
