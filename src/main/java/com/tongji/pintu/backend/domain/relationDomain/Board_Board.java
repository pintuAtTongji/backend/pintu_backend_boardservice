package com.tongji.pintu.backend.domain.relationDomain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public class Board_Board {
	@ApiModelProperty(value = "发起者id")
	private Integer userId;

	@ApiModelProperty(value = "标题")
	private String title;

	@ApiModelProperty(value = "内容")
	private String content;

	@ApiModelProperty(value = "参与人数")
	private Integer userCount;

	@ApiModelProperty(value = "图板的图片")
	private List<String> relatedImage;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content =content;
	}

	public Integer getUserCount() {
		return userCount;
	}

	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}

	public List<String> getRelatedImage() {
		return relatedImage;
	}

	public void setRelatedImage(List<String> relatedImage) {
		this.relatedImage = relatedImage;
	}
}
