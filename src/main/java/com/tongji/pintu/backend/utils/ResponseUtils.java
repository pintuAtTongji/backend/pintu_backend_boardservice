package com.tongji.pintu.backend.utils;

public class ResponseUtils {
    public static <E> Response<E> success(E element){
        Response<E> response = new Response<>();
        response.setCode(200);
        response.setMessage("success");
        response.setData(element);
        return response;
    }

    public static <E> Response<E> error(Integer code, String message){
        Response<E> response = new Response<>();
        response.setCode(code);
        response.setMessage(message);
        response.setData(null);
        return response;
    }
}
