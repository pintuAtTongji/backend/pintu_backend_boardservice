package com.tongji.pintu.backend.controller;

import com.tongji.pintu.backend.service.UserService;
import com.tongji.pintu.backend.utils.Response;
import com.tongji.pintu.backend.utils.ResponseUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class UserController {
    @Autowired
    UserService userService;
    @ApiOperation("添加用户")
    @RequestMapping(value = "addUser/{id}", method = RequestMethod.POST)
    public Response addUser(@PathVariable("id") Integer id){

        Integer result =userService.addUser(id);
        if(result==0)
            return ResponseUtils.error(-1, "用户已存在");

        return ResponseUtils.success(null);
    }
}
