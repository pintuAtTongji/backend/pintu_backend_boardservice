package com.tongji.pintu.backend.controller;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tongji.pintu.backend.domain.Board;
import com.tongji.pintu.backend.domain.News;
import com.tongji.pintu.backend.domain.UserInformation;
import com.tongji.pintu.backend.service.NewService;
import com.tongji.pintu.backend.utils.Response;
import com.tongji.pintu.backend.utils.ResponseUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

@RestController
public class NewController {
    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private NewService newService;

    @ApiOperation("获取动态 返回值relation的0代表参与 1代表发布")
    @RequestMapping(value = "/news", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "请输入页码",required = true)})
    public Response<List<News>> getNews(HttpServletRequest httpServletRequest,
                                        @RequestParam(value = "page", required = true, defaultValue = "1") Integer page){
        HttpSession session = httpServletRequest.getSession();
        Integer userId = (Integer) session.getAttribute("id");
        if (userId == null){
            return ResponseUtils.error(401, "用户未登录");
        }
        restTemplate.getMessageConverters().set(1,new StringHttpMessageConverter(StandardCharsets.UTF_8));
        System.out.println("id:"  + userId);
        String responseJson = restTemplate.getForObject
                ("http://121.199.79.177:11001/user/api/user/follow/{1}/followings",String.class, userId);
        //HashMap response = JSON.parseObject(responseJson, HashMap.class);
        //由于是嵌套对象，需要用JSON转义Object
        String myInfoJson =
                restTemplate.getForObject("http://121.199.79.177:11001/user/api/user/{1}/info",
                        String.class, userId);

//        System.out.println(responseJson);
//        System.out.println("myinfoJson" + myInfoJson);
        ObjectMapper mapper = new ObjectMapper();
        try{
            List<UserInformation> followings = JSON.parseArray(JSON.parseObject(responseJson).getString("data"), UserInformation.class);
            //取出data对象并转成object
            UserInformation myInfo = mapper.readValue(JSON.parseObject(myInfoJson).getString("data"), UserInformation.class);
            followings.add(myInfo);
            if(followings.size() == 0){
                return ResponseUtils.success(null);
            }
            return ResponseUtils.success(newService.getNews(followings, page, userId));
        } catch (NullPointerException | JsonProcessingException np){
            np.printStackTrace();
        }
        return ResponseUtils.error(500, "未知错误");
    }



}
