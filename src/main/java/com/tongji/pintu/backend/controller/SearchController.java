package com.tongji.pintu.backend.controller;

import com.tongji.pintu.backend.domain.Key;
import com.tongji.pintu.backend.domain.Board;
import com.tongji.pintu.backend.service.BoardService;
import com.tongji.pintu.backend.utils.Response;
import com.tongji.pintu.backend.utils.ResponseUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/board/search")
public class SearchController {
    @Autowired
    private BoardService boardService;

    @ApiOperation("按title搜索图板")
    @RequestMapping(value = "/title",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key",paramType = "body",dataType = "Key"),
    })
    public Response<List<Board>> searchWithTitle(@RequestBody  Key key){
        return  ResponseUtils.success(boardService.searchWithTitle(key)
        );
    }

    @ApiOperation("按content搜索图板")
    @RequestMapping(value = "/content",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key",paramType = "body",dataType = "Key"),
    })
    public Response<List<Board>> searchWithContent(@RequestBody  Key key){
        return  ResponseUtils.success(boardService.searchWithContent(key)
        );
    }

}
