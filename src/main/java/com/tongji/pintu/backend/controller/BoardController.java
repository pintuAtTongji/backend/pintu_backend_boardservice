package com.tongji.pintu.backend.controller;

import com.alibaba.fastjson.JSONObject;
import com.tongji.pintu.backend.domain.relationDomain.Board_Board;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.tongji.pintu.backend.service.BoardService;
import com.tongji.pintu.backend.domain.Board;
import com.tongji.pintu.backend.utils.Response;
import com.tongji.pintu.backend.utils.ResponseUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/board")
public class BoardController {
    @Autowired
    private BoardService boardService;

    @ApiOperation("获取指定id的图板信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "未登录时不提供isAttended、isCollected、isCreator三个KEY"),
    })
    public Response<Object> getBoard(HttpServletRequest httpServletRequest, @PathVariable("id") Integer id){
        HttpSession session = httpServletRequest.getSession();
        Integer userId = (Integer) session.getAttribute("id");
        return boardService.getBoard(userId, id);
    }

    @ApiOperation("发布图板")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Response publish(@RequestBody Board_Board board){
        return this.boardService.publish(board);
    }

    @ApiOperation("修改图板")
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="board",paramType="body",dataType = "Board_Board"),
    })
    public Response updateBoard(@PathVariable int id, @RequestBody Board_Board board){
        return boardService.updateBoard(id,board);
    }

    @ApiOperation("删除图板")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public Response deleteBoard(@PathVariable ("id") Integer id){
        Integer result =boardService.deleteBoard(id);
        switch (result){
            case -1:
                return ResponseUtils.error(-1,"无此图板");
        }
        return ResponseUtils.success(null);
    }

    @ApiOperation(("获取参加特定图板的用户信息"))
    @RequestMapping(value = "/{board_id}/attender" , method = RequestMethod.GET)
    public Response<List<JSONObject>> getAttender(@PathVariable ("board_id") Integer id){
       List<Integer> result =boardService.getAttender(id);
       List<JSONObject> attenderList =new ArrayList<>();
       for (int i=0;i<result.size();i++)
       {
           RestTemplate restTemplate = new RestTemplate();
           JSONObject userInformation = restTemplate.getForObject
                ("http://121.199.79.177:11001/user/api/user/"+result.get(i)+"/info",JSONObject.class,result.get(i));
           JSONObject infoJSON= userInformation.getJSONObject("data");
           if(infoJSON ==null) {continue;}
           JSONObject resultJSON = new JSONObject();
           resultJSON.put("userId",infoJSON.getString("userId"));
           resultJSON.put("userName",infoJSON.getString("name"));
           resultJSON.put("avatarUrl",infoJSON.getString("profilePhotoUrl"));
           attenderList.add(resultJSON);
       }
        return ResponseUtils.success(attenderList);
    }
}