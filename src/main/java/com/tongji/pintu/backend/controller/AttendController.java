package com.tongji.pintu.backend.controller;


import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import com.tongji.pintu.backend.utils.Response;
import com.tongji.pintu.backend.utils.ResponseUtils;
import com.tongji.pintu.backend.service.AttendService;
import com.tongji.pintu.backend.domain.Attend;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AttendController {
    @Autowired
    private AttendService attendService;

    @RequestMapping(value = "/attend", method = RequestMethod.POST)
    @ApiOperation("参与图板")
    @ApiImplicitParams({
            @ApiImplicitParam(name="attend",paramType="body",dataType = "Attend"),
    })
    public Response attendBoard(@RequestBody Attend attend){
        Integer result =attendService.attendBoard(attend);
        if(result==null)
            return ResponseUtils.success(null);
                    //ResultMapUtils.error(-1, "参与失败");
        switch (result){
            case -1:
                return ResponseUtils.error(-1, "参与失败");

        }
        return ResponseUtils.success(null);
    }



    @RequestMapping(value = "/exit", method = RequestMethod.POST)
    @ApiOperation("退出图板")
    @ApiImplicitParams({
            @ApiImplicitParam(name="attend",paramType="body",dataType = "Attend"),
    })
    public Response exitBoard(@RequestBody Attend attend)
    {
        Integer result1 =attendService.exitBoard(attend);
        if(result1==null)
            return ResponseUtils.error(-1, "参与失败");
        switch (result1){
            case -1:
                return ResponseUtils.error(-1, "退出失败");
        }
        return ResponseUtils.success(null);
    }
}


