package com.tongji.pintu.backend.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import com.tongji.pintu.backend.domain.Board;
import com.tongji.pintu.backend.service.BoardService;
import com.tongji.pintu.backend.utils.Response;
import com.tongji.pintu.backend.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/{id}/board")
public class UserBoardController {
	@Autowired
	private BoardService boardService;

	@ApiOperation("获取用户参加的图板")
	@RequestMapping(value = "/attend",method = RequestMethod.GET)

	public Response<List<Board>> getAttendBoard(@PathVariable("id") Integer userId){
		return ResponseUtils.success(boardService.getAttendBoard(userId));
	}

	@ApiOperation("获取用户创建的图板")
	@RequestMapping(value = "/create",method = RequestMethod.GET)

	public Response<List<Board>> getCreateBoard(@PathVariable ("id") Integer userId){
		return ResponseUtils.success(boardService.getCreateBoard(userId));
	}

	@ApiOperation("获取用户收藏的图板")
	@RequestMapping(value = "/collect",method = RequestMethod.GET)

	public Response<List<Board>> getCollectedBoard(@PathVariable ("id") Integer userId){
		return ResponseUtils.success(boardService.getCollectBoard(userId));
	}

}
