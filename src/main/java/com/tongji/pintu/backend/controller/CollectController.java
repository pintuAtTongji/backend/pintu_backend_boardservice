package com.tongji.pintu.backend.controller;


import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import com.tongji.pintu.backend.utils.Response;
import com.tongji.pintu.backend.utils.ResponseUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tongji.pintu.backend.domain.Favorite;
import com.tongji.pintu.backend.service.FavoriteService;


@RestController
public class CollectController {
    @Autowired
    private FavoriteService favoriteService;
    @ApiOperation("收藏图板")
    @RequestMapping(value = "/collect", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="favorite",paramType="body",dataType = "Favorite"),
    })
    public Response collectBoard(@RequestBody Favorite favorite){
        Integer result =favoriteService.collectBoard(favorite);
        if(result==null)
            return ResponseUtils.success(null);
        switch (result){
            case 500:
                return ResponseUtils.error(500, "收藏失败");
        }
        return ResponseUtils.success(null);
    }

    @ApiOperation("取消收藏图板")
    @RequestMapping(value = "/uncollect", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="favorite",paramType="body",dataType = "Favorite"),
    })
    public Response cancelCollection (@RequestBody Favorite favorite){
        Integer result =favoriteService.cancelCollection(favorite);
        if(result==null)
            return ResponseUtils.error(-1, "取消收藏失败");
        switch (result){
            case -1:
                return ResponseUtils.error(-1, "取消收藏失败");
        }
        return ResponseUtils.success(null);
    }
}
