package com.tongji.pintu.backend.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tongji.pintu.backend.domain.Board;
import com.tongji.pintu.backend.domain.Key;
import com.tongji.pintu.backend.domain.relationDomain.Board_Board;
import com.tongji.pintu.backend.mapper.BoardMapper;
import com.tongji.pintu.backend.mapper.CollectMapper;
import com.tongji.pintu.backend.mapper.AttendMapper;
import com.tongji.pintu.backend.utils.Response;
import com.tongji.pintu.backend.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("boardService")
public class BoardService {
    @Autowired
    private BoardMapper boardMapper;
    @Autowired
    private CollectMapper collectMapper;
    @Autowired
    private AttendMapper attendMapper;

    public Response publish(Board_Board board_board){
        JSONObject boardJsonObj = JSONObject.parseObject(JSONObject.toJSONString(board_board));
        Board board = new Board(
            boardJsonObj.getInteger("id"),
            boardJsonObj.getInteger("userId"),
            boardJsonObj.getString("title"),
            boardJsonObj.getString("content"),
            boardJsonObj.getInteger("userCount"),
            null,
            JSON.toJSONString(board_board.getRelatedImage())
        );
        return ResponseUtils.success(boardMapper.publish(board));
    }

    public Response updateBoard(Integer id,Board_Board board_board){
        JSONObject boardJsonObj = JSONObject.parseObject(JSONObject.toJSONString(board_board));
        Board board = new Board(
                id,
                boardJsonObj.getInteger("userId"),
                boardJsonObj.getString("title"),
                boardJsonObj.getString("content"),
                boardJsonObj.getInteger("userCount"),
                null,
                JSON.toJSONString(board_board.getRelatedImage())
        );
        Integer result = boardMapper.updateBoard(id,board);
        switch (result){
            case -1:
                return ResponseUtils.error(-1, "无此图板");
        }
        return ResponseUtils.success(null);
    }

    public Response<Object> getBoard(Integer userId, Integer boardId){
	    Board board = boardMapper.getBoard(boardId);
	    if(board==null){
	    	return ResponseUtils.error(400,"图板不存在，获取失败。");
	    }
        JSONObject obj = JSON.parseObject(JSONObject.toJSONString(board));
        JSONArray relatedImage=JSONArray.parseArray(obj.getString("relatedImage"));
        //userId=1;
	    if(userId!=null){
    	    Boolean isCollected = collectMapper.isCollected(userId, boardId);
            obj.put("isCollected", isCollected);
            Boolean isAttended = attendMapper.isAttended(userId, boardId);
            obj.put("isAttended", isAttended);
            if(userId==obj.getInteger("userId"))
                obj.put("isCreator",true);
            else
                obj.put("isCreator",false);
        }
       /* else{
            obj.put("isCollected", "未登录");
        }
        */
        obj.put("relatedImage", relatedImage);
        return ResponseUtils.success(obj);
    }

    public Integer deleteBoard(Integer id) {return boardMapper.deleteBoard(id);}

    public List<Board> getAttendBoard(Integer userId) {
        return  boardMapper.getAttendBoard(userId);
    }

    public List<Board> getCollectBoard(Integer userId) {return  boardMapper.getCollectBoard(userId);}

    public List<Board> getCreateBoard(Integer userId) {return  boardMapper.getCreateBoard(userId);}

    public List<Board> searchWithTitle(Key key) {return boardMapper.searchWithTitle(key);}

    public List<Board> searchWithContent(Key key) {return boardMapper.searchWithContent(key);}

    public List<Integer> getAttender (Integer id) {return boardMapper.getAttender(id);}
}
