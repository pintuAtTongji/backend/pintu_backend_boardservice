package com.tongji.pintu.backend.service;

import com.tongji.pintu.backend.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserService {
    @Autowired
    private UserMapper userMapper;
    public Integer addUser(int id){
        if (userMapper.isExist(id))
            return 0;
        else
        {
            userMapper.addUser(id);
            return 1;
        }
      }

}
