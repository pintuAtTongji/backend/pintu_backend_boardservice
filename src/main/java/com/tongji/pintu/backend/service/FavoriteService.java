package com.tongji.pintu.backend.service;

import com.tongji.pintu.backend.domain.Favorite;
import com.tongji.pintu.backend.mapper.CollectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("favoriteService")
public class FavoriteService {
    @Autowired
    private CollectMapper collectMapper;

    public Integer collectBoard(Favorite favorite){return  collectMapper.collectBoard(favorite); }

    public Integer cancelCollection(Favorite favorite){return  collectMapper.cancelCollection(favorite); }
}
