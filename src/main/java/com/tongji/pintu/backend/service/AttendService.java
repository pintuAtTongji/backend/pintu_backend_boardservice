package com.tongji.pintu.backend.service;

import com.tongji.pintu.backend.domain.Attend;
import com.tongji.pintu.backend.mapper.AttendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("attendService")
public class AttendService {
    @Autowired
    private AttendMapper attendMapper;
    public Integer exitBoard(Attend attend){return  attendMapper.exitBoard(attend); }
    public Integer attendBoard(Attend attend){return  attendMapper.attendBoard(attend); }


}
