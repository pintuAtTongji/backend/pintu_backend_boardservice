package com.tongji.pintu.backend.service;

import com.tongji.pintu.backend.domain.Board;
import com.tongji.pintu.backend.domain.News;
import com.tongji.pintu.backend.domain.UserInformation;
import com.tongji.pintu.backend.mapper.NewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("newService")
public class NewService {
    @Autowired
    private NewMapper newMapper;

    private final Integer pageSize = 5;

    public List<News> getNews(List<UserInformation> infoList, Integer page, Integer userId){
        Map<Integer, List<String>> map = new LinkedMultiValueMap<>();
        List<Integer> idList = new ArrayList<>();
        for(UserInformation info : infoList){
            List<String> valueList = new ArrayList<>();
            valueList.add(info.getName());
            valueList.add(info.getProfilePhotoUrl());
            map.put(info.getUserId(), valueList);
            idList.add(info.getUserId());
        }
        List<News> result = newMapper.getNews(idList, (page - 1) * pageSize, pageSize);
        System.out.println("news:" + result.size());
        for(News news : result){
            //System.out.println();
            news.setName(map.get(news.getUserId()).get(0));
            news.setProfilePhotoUrl(map.get(news.getUserId()).get(1));
        }

        return result;
    }
}
