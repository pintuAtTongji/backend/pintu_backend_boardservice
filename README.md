## APIs

### 用户部分

```
根据id查询用户
GET		/api/v1/user/{user_id}

注册用户
POST	/api/v1/user
	参数:
	{
        'userName': '张三',
        'password': '123456'
    }

更改用户名信息
POST	/api/v1/user/{user_id}
	参数：
	{
        'userName': '张三',		//optional
        'password': '123456'		//optional
    }

获取用户参加的所有项目列表
GET		/api/v1/user/{user_id}/project

用户参加某个项目
POST	/api/v1/user/{user_id}/project/{project_id}
	参数：
	{
		'inCharge':false   //optinal, true表示是项目的管理者，false表示不是项目的管理者。默认是false。
	}
	
用户退出某个项目
DELETE	/api/v1/user/{user_id}/project/{project_id}
```

### 项目部分

```
新建项目
POST	/api/v1/project
	参数：
    {
        "projectName": "设计模式小组项目",
        "creatorId": "1"
    }

获取某个项目信息
GET		/api/v1/project/{project_id}
	返回：
	{
		//其它信息
		"states": ["状态1","状态2","状态3"]	//注意states从这里获取
	}

修改某个项目信息
POST	/api/v1/project/{project_id}
	参数：
	{
		"projectName": "项目新名字"	//optional
		“states”:['新状态1','新状态2']	//optional
	}
	
删除项目
DELETE	/api/v1/project/{project_id}

获取项目的所有用户
GET		/api/v1/project/{project_id}/user

获取项目的所有任务
GET		/api/v1/project/{project_id}/task

增加项目任务
POST	/api/v1/project/{project_id}/task
	参数：
	{
		"state": "正在开发"
	}

删除项目任务
DELETE	/api/v1/project/{project_id}/task/{task_id}

获取项目的所有文件(todo)
GET		/api/v1/project/{project_id}/file
```

### 任务部分

```
获取某个任务
GET		/api/v1/task/{task_id}

修改某个任务
POST	/api/v1/task/{task_id}
	参数：//全部可选
	{
		"title": "任务的主题",
		"description": "任务的描述",
		"relatedUser": ["任务的负责人1","任务的负责人2"],
		"timeStart": "2019-10-1 15:00:00",
		"timeEnd": "2019-10-7 18:30:00",
	}
```

### 文件部分(todo)

```
上传某个文件
POST	/api/v1/file
	参数：
	{
		//TODO
	}

获取某个文件
GET		/api/v1/file/{file_id}

删除某个文件
DELETE	/api/v1/file/{file_id}
```

